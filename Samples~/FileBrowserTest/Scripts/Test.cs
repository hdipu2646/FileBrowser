﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Walid.System.IO;

public class Test : MonoBehaviour
{
    public Camera SnapshotCamera;
    public void Browse()
    {
        FileBrowser.Singleton.Browse(FileType.csv);
    }

    public void LoadFile(FileInfo data)
    {
        Debug.Log( data.fileData.Length);
    }

    public void DownloadSC()
    {
        Texture2D texture = new Texture2D(1920, 1080, TextureFormat.ARGB32, false);
        Rect grab_area = new Rect(0, 0, 1920, 1080);
        RenderTexture.active = SnapshotCamera.targetTexture;
        texture.ReadPixels(grab_area, 0, 0);
        SnapshotCamera.Render();
        texture.Apply();
        byte[] bytes = ImageConversion.EncodeToJPG(texture);
        FileBrowser.Singleton.DownloadFile(FileType.jpg, DateTime.Now.Minute.ToString(), bytes);
    }
}
