﻿using System;
using System.Collections;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace Walid.System.IO
{
    public enum FileType
    {
        zip,
        gif,
        csv,
        jpg,
        png,
        txt,
        json,
        pdf,
        mp3,
        wav,
        docx,
        pptx,
        doc,
        ppt
    }

    #region File Selection
    [Serializable]
    public struct SelectedFileBlobInfo
    {
        public string fileName, blobURL;
    }

    [Serializable]
    public struct FileInfo
    {
        public byte[] fileData;
        public string fileName;
    }

    [Serializable]
    public class FileSelectionEvent : UnityEvent<FileInfo> { }
    #endregion File Selection

    public class FileBrowser : MonoBehaviour
    {
        public FileSelectionEvent OnFileSelect;
        public UnityEvent OnError;
        #region Singleton
        private static FileBrowser instance;
        public static FileBrowser Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<FileBrowser>();
                    if (instance == null)
                    {
                        GameObject webGLFileBrowser = new GameObject();
                        webGLFileBrowser.name = typeof(FileBrowser).Name;
                        instance = webGLFileBrowser.AddComponent<FileBrowser>();
                        DontDestroyOnLoad(webGLFileBrowser);
                    }
                }
                return instance;
            }
        }

        public static void SetNewSingleTon(FileBrowser webGLFileBrowser)
        {
            if (instance != null)
            {
                return;
            }
            instance = webGLFileBrowser;
        }
        #endregion Singleton

        public void WebGLReadFileData(string fileInfo)
        {
            StartCoroutine(ReadFileData(JsonUtility.FromJson<SelectedFileBlobInfo>(fileInfo)));
        }

        public void Browse(FileType fileType)
        {
#if UNITY_EDITOR
            string fileLocation = EditorUtility.OpenFilePanel(title: string.Format("Upload your CSV"), "", extension: string.Format("{0}", fileType));
            SelectedFileBlobInfo fileBlobInfo = new SelectedFileBlobInfo();
            string[] locationInfo = fileLocation.Split('/');
            fileBlobInfo.fileName = locationInfo[locationInfo.Length - 1];
            fileBlobInfo.blobURL = fileLocation;
            Singleton.WebGLReadFileData(JsonUtility.ToJson(fileBlobInfo));
#elif UNITY_WEBGL && !UNITY_EDITOR
            JavascriptManager.UploadSingleFile(fileTypePTR: string.Format(".{0}", fileType), Singleton.gameObject.name, "WebGLReadFileData");
#endif
        }

        public void DownloadFile(FileType fileType, string fileName, byte[] bytes)
        {
#if UNITY_EDITOR
            string path = EditorUtility.SaveFilePanel(title: string.Format("Saving {0}", fileName), directory: string.Empty, defaultName: fileName,extension: fileType.ToString()); //Application.persistentDataPath + "/" + fileName + "." + fileType;
            Debug.Log(path);
            File.WriteAllBytes(path, bytes);
#elif UNITY_WEBGL && !UNITY_EDITOR
            JavascriptManager.DownloadSingleFile(fileTypePTR: string.Format(".{0}", fileType), fileNamePTR: fileName, byteArray: bytes, byteArraySize: bytes.Length, WebGLGameObjectNamePTR: Singleton.gameObject.name, WebGLMethodPTR: "WebGLReadFileData");
#endif
        }

        private IEnumerator ReadFileData(SelectedFileBlobInfo fileBlobInfo)
        {
            using (UnityWebRequest uwr = UnityWebRequest.Get(fileBlobInfo.blobURL))
            {
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = Convert.ToInt64(uwr.GetResponseHeader("Content-Length"));
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                while (!asyncOperation.isDone)
                {
                    float progress = uwr.uploadProgress;
                    progress *= 100f;
                    double uploadedSize = uwr.uploadedBytes;
                    yield return new WaitForEndOfFrame();
                }
                Action callBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError or UnityWebRequest.Result.ProtocolError or UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        OnError?.Invoke();
                    },
                    UnityWebRequest.Result.Success => () =>
                    {
                        FileInfo fileInfo = new FileInfo();
                        fileInfo.fileData = uwr.downloadHandler.data;
                        fileInfo.fileName = fileBlobInfo.fileName;
                        OnFileSelect?.Invoke(fileInfo);
                        OnFileSelect.RemoveAllListeners();
                    },
                    _ => () =>
                    {
                        OnError?.Invoke();
                    },
                };
                callBack?.Invoke();
                uwr.Dispose();
            }
        }
    }
}